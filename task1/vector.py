class Vector:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __repr__(self):
        return f"Vector({self.x}, {self.y})"

    def __add__(self, other):  #sčítání vektorů
        return Vector(self.x + other.x, self.y + other.y)

    def __sub__(self, other): # odčítání vektorů
        return Vector(self.x - other.x, self.y - other.y)

    def __mul__(self, scalar): # násobení vektorů
        return Vector(self.x * scalar, self.y * scalar)

    def __eq__(self, other): # porovnání vektorů
        return self.x == other.x and self.y == other.y
