class Node:
    def __init__(self, data=None):
        self.data = data
        self.prev = None
        self.next = None

class DoublyLinkedList:
    def __init__(self):
        self.head = None
        self.tail = None
        self.length = 0

    def __len__(self):
            return self.length

    def append(self, data):
        new_node = Node(data)
        if not self.head:
            self.head = new_node
        else:
            self.tail.next = new_node
            new_node.prev = self.tail
        self.tail = new_node
        self.length += 1
    
    def __getitem__(self, index):
        if not (-self.length <= index < self.length):
            raise IndexError("Index out of range")
        elif index < 0:
            index += self.length
        current = self.head
        for _ in range(index):
            current = current.next
        return current.data

    def __setitem__(self, index, data):
        if not (-self.length <= index < self.length):
            raise IndexError("Index out of range")
        elif index < 0:
            index += self.length
        current = self.head
        for _ in range(index):
            current = current.next
        current.data = data

    def __delitem__(self, index):
        if not (-self.length <= index < self.length):
            raise IndexError("Index out of range")
        elif index < 0:
            index += self.length
        elif index == 0:
            if self.head == self.tail:
                self.head = self.tail = None
            else:
                self.head = self.head.next
                self.head.prev = None
        elif index == self.length - 1:
            self.tail = self.tail.prev
            self.tail.next = None
        else:
            current = self.head
            for _ in range(index):
                current = current.next
            current.prev.next = current.next
            current.next.prev = current.prev
        self.length -= 1

    def __iter__(self):
        current = self.head
        data_list = []
        while current:
            data_list.append(current.data)
            current = current.next
        return iter(data_list)
