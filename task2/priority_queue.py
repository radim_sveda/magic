class PriorityQueue:
    def __init__(self):
        self.queue = []

    def __iter__(self):
        sorted_queue = sorted(self.queue, key=lambda x: x[0])  # Seřazení dle priorit od nejnižší k nejvyšší
        return iter([task for _, task in sorted_queue])
    
    def __len__(self):
        return len(self.queue)
    
    def pop(self):
        if not self.queue:
            raise IndexError("pop from a priority queue that is empty")
        self.queue.sort()  # Seřazení od nejnižšího k nejvyššímu
        return self.queue.pop(0)[1]  # Vrací se první prvek s nejnižší prioritu
    
    def push(self, task, priority):
        self.queue.append((priority, task))
